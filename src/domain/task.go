package domain

type Task struct{
	Id string `json:"id"`
	Title string `json:"title"`
	Description string `json:"description"`
}

type TargetCommandRepo interface {
	// The Get method looks up a single task by id.
	Get(id string) (*Task, error)

	// The GetAll method lists all tasks.
	GetAll() ([]Task, error)

	// The Put method adds a single task, and returns the task's id.
	Put(Task) (id string, err error)

	// The Delete method deletes a single task by id.
	Delete(id string) error
}


